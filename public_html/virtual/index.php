<?php
$password = "salita";
if ($_POST['password'] != $password) {
?>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Accesos a Reuniones</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="styles.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    </head>

    <body>
        <!--Contenido-->
        <div class="login-screen">
            <div class="content">
                <div class="content-padding">
                    <h2>Comunidad de Villa Crespo</h2>
                    <span>Acceso a los links de las reuniones.</span>
                    <br><br>
                    <span>La clave es la misma que usamos siempre en las reuniones. Si no te acordás pedí la clave por whatsapp.</span>
                    <br><br>
                    <!--Fin de Contenido-->
    
                    <form name="form" method="post" action="">
                        <span class="text-bold">Contraseña:</span>
                        <input type="password" name="password"><br><br>
                        <input type="submit" value="Acceder">
                    </form>
                </div>
            </div>
            <img class="img-sala" src="../images/sala.svg" alt="">
        </div>
    <?php
} else {
    ?>

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>Accesos a Reuniones</title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="styles.css">
            <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
        </head>

        <!--Contenido-->
        <div class="content wide">
            <div class="content-padding">  
                <p>Para Ceremonias Grabadas ir a:  <a href="https://elmensajedesilo.com/villacrespo/" target="_blank">elmensajedesilo.com/villacrespo</a></p>                 
            </div>
        </div>
        <div class="content wide">
            <div class="content-padding">
                <h2>Martes de Experiencia</h2>
                <span>Todos los Martes a las 19:00hs.</span>
                <p>ID: <span class="text-bold">853 2795 9467</span></p>
                <p>Contraseña: <span class="text-bold">salita</span></p>
            </div>
            <div class="link-container">
                <span class="text-center">Link Zoom</span>
                <a href="https://us02web.zoom.us/j/85327959467?pwd=THhwQ1cvSDY4NjNkeU9kak5XS2s4dz09">
                    https://us02web.zoom.us/j/85327959467?pwd=THhwQ1cvSDY4NjNkeU9kak5XS2s4dz09</a>
            </div>
        </div>

        <div class="content wide">
            <div class="content-padding">
                <h2>Viernes de Estudio</h2>
                <span>Quincenal a las 19:00hs.</span>
                <p>ID: <span class="text-bold">857 7707 8818</span></p>
                <p>Contraseña: <span class="text-bold">salita</span></p>
            </div>

            <div class="link-container">
                <span class="text-center">Link Zoom</span>
                <a href="https://us02web.zoom.us/j/85777078818?pwd=d3ZKM29Ia2pmZm9nSzZBWmUreWRGZz09">
                    https://us02web.zoom.us/j/85777078818?pwd=d3ZKM29Ia2pmZm9nSzZBWmUreWRGZz09</a>
            </div>
        </div>

        <div class="content wide">
            <div class="content-padding">
                <h2>Conjunta Internacional</h2>
                <span>Todos los días a las 17:00hs.</span>
                <p>ID: <span class="text-bold">831 8030 8909</span></p>
                <p>Contraseña: <span class="text-bold">04M69</span></p>
                <div class="ao-vivo">
                    <span>
                        <a href="https://www.youtube.com/pazfuerzayalegria" target="_blank">En Vivo (Youtube)</a>
                    </span>
                    <span>
                        <a href="https://www.facebook.com/pazfuerzayalegria/" target="_blank">En Vivo (Facebook)</a>
                    </span>                    
                </div>
            </div>

            <div class="link-container">
                <span class="text-center">Link Zoom</span>
                <a href="https://us02web.zoom.us/j/83180308909">
                https://us02web.zoom.us/j/83180308909</a>
            </div>
        </div>

        <!--Fin de Contenido-->
    </body>



<?php
}
?>