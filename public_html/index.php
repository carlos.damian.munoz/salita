<?php
$mail_class   = " success";
if (!empty($_POST)) {
    //captura de variables
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    //cuerpo del email
    $message_to_me = "
  <strong>Nombre:</strong> $name <br /><br />
  <strong>Email:</strong> $email <br /><br />
  <strong>Mensaje:</strong> $message <br /><br />
  ";
    //envío del email
    $mail_sent = mail("elmensajedesilo.villacrespo@gmail.com", "Consulta Web", $message_to_me, "From: $name <$email>\nReply-To:$email\nContent-Type:text/html; charset=utf-8\n");
    if ($mail_sent) {
        $mensaje_mail = "Tu mensaje se envió correctamente!";
        $mail_class = "success";
    } else {
        $mensaje_mail = "Tu mensaje no se pudo enviar";
        $mail_class = "fail";
    }
}
?>
<!DOCTYPE html>
<?php if (isset($mensaje_mail)) { ?>
<div id="mail-message" class=" <?php echo $mail_class; ?>"><?php echo $mensaje_mail; ?></div>
<?php } ?>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>El Mensaje de Silo Villa Crespo</title>
  <meta name="description" content="El Mensaje de Silo Villa Crespo">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--opengraph-->
  <meta property="og:image" content="https://elmensajedesilo.com/villacrespo/images/og_pic.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="256">
  <meta property="og:image:height" content="256">
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://elmensajedesilo.com/villacrespo" />
  <meta property="og:title" content="Comunidad de Villa Crespo" />
  <meta property="og:description" content="El Mensaje de Silo." />
  <!--external-->
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet'>
  <!--Reset Audio Player-->
  <link rel="stylesheet" href="https://cdn.plyr.io/3.5.10/plyr.css" />
  <link rel="stylesheet" href="custom_player.css">
</head>

<body>
  <!--header-->
  <header>
    <div class="container row full-height">
      <div>
        <a href="https://elmensajedesilo.com/villacrespo"><img class="img-logo" src="images/silo.svg"
            alt="Logo: El Mensaje de Silo Villa Crespo"></a>
      </div>
      <div>
        <h1>El Mensaje de Silo</h1>
        <h2>Comunidad de Villa Crespo</h2>
      </div>
    </div>
  </header>
  <!--nav
  <nav class="container">
    <ul>
      <li>Inicio</li>
      <li>Reuniones</li>
      <li>Dirección</li>
      <li>Mapa</li>
      <li>Contacto</li>
    </ul>
  </nav>
  -->
  <!--main-->
  <main>
    <!--frase y sala-->
    <section class="bg-blue">
      <div class="container row">
        <div class="column white">
          <q>Cuando encuentres una gran fuerza, alegría y bondad en tu corazón, o cuando te sientas libre y sin
            contradicciones, inmediatamente agradece en tu interior.
            <br> Cuando te suceda lo contrario pide con fe y aquel agradecimiento que acumulaste volverá convertido y
            ampliado en beneficio.</q>
          <br><br>
          <p>Silo</p>
        </div>
        <div class="column">
          <img class="img-stretch" src="images/sala.svg" alt="eliminar">
        </div>
      </div>
    </section>
    <!--Ceremonias-->
    <section class="bg-media-section">
      <div class="container row align-top">
        <div class="column">
          <h2><i class="fa fa-headphones fa-lg" aria-hidden="true"></i> Ceremonias en Audio</h2>
          <br>
          <div class="audio-player">
            <!--Bienestar-->
            <div class="ceremonia-type">
              <h3>Bienestar</h3>
              <!--track-->
              <div class="track-container">
                <div class="track-title-bg">
                  <p class="track-title">Angel De Natale - Tamara Ferrer</p>
                </div>
                <audio controls preload=none>   
                  <source src="https://drive.google.com/uc?export=download&id=1wk7Lbm1h_HTVLhZ9iKIYlB9UDFy9SvS2"
                    type="audio/mp3">
                  <source src="/audios/Bienestar_Angel_de_Natale_Tamara_Ferrer.mp3">
                </audio>
              </div>
              <!--track-->
              <div class="track-container">
                <div class="track-title-bg">
                  <p class="track-title">Angel Guerra - Angel De Natale</p>
                </div>
                <audio controls preload=none>
                  <source src="https://drive.google.com/uc?export=download&id=1ynnxa_lyp7U3Eg_OoOvBcXMjeOR_8OK2"
                    type="audio/mp3">
                    <source src="/audios/Bienestar_Angel_Guerra_Angel_de_Natale.mp3">
                </audio>
              </div>
              <!--track-->
              <div class="track-container">
                <div class="track-title-bg">
                  <p class="track-title">Carlos Muñoz - Tamara Ferrer</p>
                </div>
                <audio controls preload=none>
                  <source src="https://drive.google.com/uc?export=download&id=1EKWrEZVIAZBI8f3pa_C59Yin3A-Gg6bl"
                    type="audio/mp3">
                  <source src="/audios/Bienestar_Carlos_Munoz_Tamara_Ferrer.mp3">
                </audio>
              </div>
              <!--track-->
              <div class="track-container">
                <div class="track-title-bg">
                  <p class="track-title">Luis Beltrán - Angel Guerra</p>
                </div>
                <audio controls preload=none>
                  <source src="https://drive.google.com/uc?export=download&id=17ne0HxOcTLNKlf8ssPepXhmozGtStf-A"
                    type="audio/mp3">
                    <source src="/audios/Bienestar_Luis_Beltran_Angel_Guerra.mp3">
                </audio>
              </div>
            </div>
            <!--Oficios-->
            <div class="ceremonia-type">
              <h3>Oficio</h3>
              <!--track-->
              <div class="track-container">
                <div class="track-title-bg">
                  <p class="track-title">Gladis Delgado - Luis Beltrán</p>
                </div>
                <audio controls preload=none>
                  <source src="https://drive.google.com/uc?export=download&id=1GX9XWEFjVsNqQCioTstPxTB5-GIfhBEC"
                    type="audio/mp3">
                    <source src="/audios/Oficio_Gladis_Delgado_Luis_Beltran.mp3">
                </audio>
              </div>
              <!--track-->
              <div class="track-container">
                <div class="track-title-bg">
                  <p class="track-title">Luis Beltrán - Gladis Delgado</p>
                </div>
                <audio controls preload=none>
                  <source src="https://drive.google.com/uc?export=download&id=1pyUl3-CR01MZyUU7nZzDjexbguHZ5BOZ"
                    type="audio/mp3">
                    <source src="/audios/Oficio_Luis_Beltran_Gladis_Delgado.mp3">
                </audio>
              </div>
            </div>
          </div>
        </div>
        <div class="column">
          <h2><i class="fa fa-youtube fa-lg" aria-hidden="true"></i> Inauguración del Parque La Reja</h2>
          <br>
          <iframe width="100%" height="320" src="https://www.youtube.com/embed/fhkN3NYEpBA" frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </section>
    <!--dias de reunion y libro-->
    <section>
      <div class="container row">
        <div class="column">
          <img class="img-book" src="images/libro.png" alt="eliminar">
        </div>
        <div class="column">
          <h2>Todas las semanas nos reunimos para compartir una experiencia.</h2>
          <br>
          <h3>Martes: 19 a 21 hs.</h3>
          <h3>Jueves: 19 a 21 hs.</h3>
          <br>
          <p>*Excepto los feriados.</p>
          <br>
          <p class="covid"><i class="fa fa-info-circle fa-lg" aria-hidden="true"> </i> Debido a la cuarentena no estamos
            realizando reuniones actualmente.</p>
          <br>
          <a class="dark-link" href="https://elmensajedesilo.com" target="blank"><i class="fa fa-file-o fa-lg"
              aria-hidden="true"> </i> Accedé a la versión digital del libro</a>
        </div>
      </div>
    </section>
    <!--dirección-->
    <section class="bg-blue">
      <div class="container row">
        <div class="column white center-text">
          <h1 class="white">Antezana 340</h1>
          <br>
          <p>Asociación Vecinal Benito Nazar</p>
          <p>Villa Crespo, Buenos Aires</p>
          <br><br><br>
          <img id="fb-icon" src="images/ico_fb.png" alt="facebook icon"><br>
          <a class="link" href="https://www.facebook.com/elmensajedesilo.villacrespo"
            target="_blank">/elmensajedesilo.villacrespo</a>
          <br><br>
          <img id="fb-icon" src="images/ico_gm.png" alt="gmail icon"><br>
          <a class="link" href="mailto:elmensajedesilo.villacrespo@gmail.com">elmensajedesilo.villacrespo@gmail.com</a>
        </div>
        <div class="column">
          <h2 class="white center-text">Contacto:</h2>
          <form action="index.php" method="post">
            <label>
              <p>Nombre: </p>
              <input type="text" name="name" placeholder="Escribe tu nombre" required>
            </label>
            <label>
              <p>Email: </p>
              <input type="email" name="email" placeholder="Escribe tu email" required>
            </label>
            <label>
              <p>Mensaje: </p>
              <textarea type="textarea" name="message" placeholder="Escribe tu mensaje"></textarea>
            </label>
            <input type="submit" name="submit" value="enviar">
          </form>
        </div>
      </div>
    </section>
    <!--mapa interactivo-->
    <section>
      <div id="map">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3283.9498189894284!2d-58.44569078760896!3d-34.605430408991396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcca0df0361e3b%3A0x8bed70a8d835e522!2sAsociaci%C3%B3n+Benito+Nazar!5e0!3m2!1ses-419!2sar!4v1537057239171"
          width="100%" height="400" zoom="150%"></iframe>
      </div>
    </section>
  </main>
  <!--footer-->
  <footer>
    <div class="container row">
      <div class="column center-text">
        <p>El Mensaje está inspirado en la obra y enseñanza de Silo. Para información más general sobre El Mensaje y
          sus comunidades te invitamos a visitar el sitio oficial de habla hispana: <a class="link"
            href="http://www.elmensajedesilo.net" target="_blank">www.elmensajedesilo.net</a>, y a la bibliografía
          oficial de Silo en: <a class="link" href="http://www.silo.net" target="_blank">www.silo.net</a></p>
      </div>
    </div>
    <div class="container column">
      <p class="center-text white">El Mensaje de Silo, Comunidad de Villa Crespo. Buenos Aires, Argentina.</p>
    </div>
  </footer>
  <!--Javascript-->
  <script src="functions.js"></script>
  <script src="https://cdn.plyr.io/3.5.10/plyr.js"></script>
  <script>
    Array.from(document.getElementsByTagName('audio')).forEach(a => new Plyr(a));
  </script>
</body>